package co.edu.misena.projectsena.db.entity

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["id"])
data class TorneoEntity (
    @field:SerializedName("id")
    val id: Int,
    @field:SerializedName("nombreTorneo")
    val nombreTorneo: String,
    @field:SerializedName("codigoTorneo")
    val codigoTorneo: String,
    @field:SerializedName("userTorneo")
    val userTorneo: String
)
