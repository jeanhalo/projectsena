package co.edu.misena.projectsena.ui.equipo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.databinding.EquipoItemBinding
import co.edu.misena.projectsena.databinding.TorneoItemBinding

class EquipoAdapter(@Nullable clickCallback: EquipoClickCallback) :
    RecyclerView.Adapter<EquipoAdapter.EquipoViewHolder>() {

    var mEquipoList: List<Equipo?>? = null

    @Nullable
    private var mEquipoClickCallback: EquipoClickCallback? = null

    init {
        mEquipoClickCallback = clickCallback
    }

    fun setEquipoList(equipoList: List<Equipo?>) {
        if (mEquipoList == null) {
            mEquipoList = equipoList
            notifyItemRangeInserted(0, equipoList.size)
        } else {

            val result: DiffUtil.DiffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return mEquipoList!![oldItemPosition]!!.id == equipoList[newItemPosition]?.id
                }

                override fun getOldListSize(): Int {
                    return mEquipoList!!.size
                }

                override fun getNewListSize(): Int {
                    return equipoList.size
                }

                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int
                ): Boolean {
                    val newEquipo: Equipo? = equipoList[newItemPosition]
                    val oldEquipo: Equipo? = mEquipoList!![oldItemPosition]
                    return (newEquipo?.id == oldEquipo?.id && newEquipo?.nombreEquipo == oldEquipo?.nombreEquipo
                            && newEquipo?.torneoId == oldEquipo?.torneoId)
                }

            })
            mEquipoList = equipoList
            result.dispatchUpdatesTo(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EquipoViewHolder {
        val binding: EquipoItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.equipo_item,
            parent,
            false
        )
        binding.callback = mEquipoClickCallback
        return EquipoViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (mEquipoList == null) 0 else mEquipoList!!.size
    }

    override fun onBindViewHolder(holder: EquipoViewHolder, position: Int) {
        holder.binding.equipo = mEquipoList?.get(position)
        holder.binding.executePendingBindings()
    }

    // Init ViewHolder
    class EquipoViewHolder(binding: EquipoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val binding: EquipoItemBinding = binding
    }
}