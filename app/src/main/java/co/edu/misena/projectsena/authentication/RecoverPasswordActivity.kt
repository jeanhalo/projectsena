package co.edu.misena.projectsena.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import co.edu.misena.projectsena.R
import com.google.firebase.auth.FirebaseAuth

class RecoverPasswordActivity : AppCompatActivity() {
    private lateinit var txtEmail : EditText
    private lateinit var auth : FirebaseAuth
    private lateinit var progressBar: ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recover_password)
        txtEmail = findViewById(R.id.txtRecoverEmail)
        auth = FirebaseAuth.getInstance()
        progressBar = findViewById(R.id.progressBarRecoverPassword)
    }

    fun sendPassword (view: View){
        val email = txtEmail.text.toString()

        if(!TextUtils.isEmpty(email)){
            auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(this){
                    task -> if (task.isSuccessful){
                    progressBar.visibility=View.VISIBLE
                    Toast.makeText(this,"Correo Enviado ",Toast.LENGTH_LONG).show()
                    startActivity(Intent(this,LoginActivity::class.java))
                }
                    else {
                    Toast.makeText(this,"Error el correo ingresado no existe",Toast.LENGTH_LONG).show()
                }
                }
        }

    }
}
