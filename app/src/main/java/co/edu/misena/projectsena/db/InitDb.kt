package co.edu.misena.projectsena.db

import androidx.room.Database
import androidx.room.RoomDatabase
import co.edu.misena.projectsena.db.dao.TorneoDao
import co.edu.misena.projectsena.db.dao.UserDao
import co.edu.misena.projectsena.db.entity.TorneoEntity
import co.edu.misena.projectsena.db.entity.User

/**
 * Main database description.
 */
@Database(
    entities = [
        User::class,
        TorneoEntity::class
    ],
    version = 8
)
abstract class InitDb : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun torneoDao(): TorneoDao
}
