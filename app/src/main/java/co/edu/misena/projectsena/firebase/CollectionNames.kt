package co.edu.misena.projectsena.firebase

class CollectionNames {
    companion object {
        const val TORNEOS = "Torneos"
        const val EQUIPOS = "Equipos"
        const val SORTEOS = "Sorteos"
    }
}