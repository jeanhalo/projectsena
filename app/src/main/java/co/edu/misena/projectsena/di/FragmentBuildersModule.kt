package co.edu.misena.projectsena.di

import co.edu.misena.projectsena.ui.crearequipo.CrearEquipoFragment
import co.edu.misena.projectsena.ui.creartorneo.CrearTorneoFragment
import co.edu.misena.projectsena.ui.equipo.EquipoFragment
import co.edu.misena.projectsena.ui.gestionartorneo.GestionarTorneoFragment
import co.edu.misena.projectsena.ui.sorteo.SorteoFragment
import co.edu.misena.projectsena.ui.torneo.TorneoListFragment
import co.edu.misena.projectsena.ui.user.UserDetailFragment
import co.edu.misena.projectsena.ui.user.UserFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeUserFragment(): UserFragment

    @ContributesAndroidInjector
    abstract fun contributeUserDetailFragment(): UserDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeTorneoListFragment(): TorneoListFragment

    @ContributesAndroidInjector
    abstract fun contributeCrearTorneoFragment(): CrearTorneoFragment

    @ContributesAndroidInjector
    abstract fun contributeGestionarTorneoFragment(): GestionarTorneoFragment

    @ContributesAndroidInjector
    abstract fun contributeEquipoFragment(): EquipoFragment

    @ContributesAndroidInjector
    abstract fun contributeCrearEquipoFragment(): CrearEquipoFragment

    @ContributesAndroidInjector
    abstract fun contributeSorteoFragment(): SorteoFragment

}
