package co.edu.misena.projectsena.ui.gestionartorneo

import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Torneo

interface GestionarTorneosClickCallback {
    fun onClick(torneo: Torneo)
    fun onNavigateEquipos(torneo: Torneo)
    fun onNavigateSorteo(torneo: Torneo)
    fun onBack()
}