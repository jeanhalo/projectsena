package co.edu.misena.projectsena.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Resource
import co.edu.misena.projectsena.data.vo.Status
import co.edu.misena.projectsena.databinding.UserDetailBinding
import co.edu.misena.projectsena.db.entity.User
import co.edu.misena.projectsena.di.Injectable
import co.edu.misena.projectsena.repository.UserRepository
import javax.inject.Inject

class UserDetailFragment : Fragment(), Injectable {

    companion object {
        const val KEY_USER_LOGIN = "user_login"
        /** Creates user detail fragment for specific user Login  */
        fun forUserLogin(userLogin: String): UserDetailFragment? {
            val fragment = UserDetailFragment()
            val args = Bundle()
            args.putString(KEY_USER_LOGIN, userLogin)
            fragment.arguments = args
            return fragment
        }

    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var viewModel: UserDetailViewModel

    private var mBinding: UserDetailBinding? = null

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate this data binding layout
        mBinding = DataBindingUtil.inflate(inflater, R.layout.user_detail, container, false)
        return mBinding!!.root
    }

    override fun onActivityCreated(@Nullable savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var paramLogin = arguments?.getString(KEY_USER_LOGIN)

        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(UserDetailViewModel::class.java)

        viewModel.setLogin(paramLogin)

        mBinding?.userDetailViewModel = viewModel

        subscribeToModel(viewModel)
    }

    private fun subscribeToModel(model: UserDetailViewModel) {

        // Observe user data
        model.getObservableUser()?.observe(viewLifecycleOwner, object : Observer<Resource<User>> {
            override fun onChanged(@Nullable userResource: Resource<User>?) {
                if (userResource?.status == Status.SUCCESS) {
                    model.setUser(userResource.data)
                    mBinding!!.isLoading = false
                } else {
                    mBinding!!.isLoading = true
                }
            }
        })


    }

}