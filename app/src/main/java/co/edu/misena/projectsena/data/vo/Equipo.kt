package co.edu.misena.projectsena.data.vo

data class Equipo (
    var id: String = "",
    val nombreEquipo: String = "",
    val torneoId: String = ""
)