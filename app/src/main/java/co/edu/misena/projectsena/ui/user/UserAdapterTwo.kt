package co.edu.misena.projectsena.ui.user

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.databinding.UserItemBinding
import co.edu.misena.projectsena.db.entity.User
import co.edu.misena.projectsena.ui.common.DataBoundListAdapter

class UserAdapterTwo(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val userClickCallback: ((User) -> Unit)?
) : DataBoundListAdapter<User, UserItemBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.login == newItem.login
                    && oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.type == newItem.type
                    && oldItem.url == newItem.url
        }

    }
) {
    override fun createBinding(parent: ViewGroup): UserItemBinding {
        val binding = DataBindingUtil.inflate<UserItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.user_item,
            parent,
            false,
            dataBindingComponent
        )
        binding.root.setOnClickListener {
            binding.user?.let {
                userClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: UserItemBinding, item: User) {
        binding.user = item
    }

}