
package co.edu.misena.projectsena.authentication

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import co.edu.misena.projectsena.MainActivity
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.MyPreferences
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


lateinit var user : String
class LoginActivity : AppCompatActivity() {

    private lateinit var txUser: EditText

    private lateinit var txtPassword: EditText
    private lateinit var progressBar : ProgressBar
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        txUser= findViewById(R.id.txtUser)
        txtPassword= findViewById(R.id.txtPassword)
        progressBar = findViewById(R.id.progressBar2)
        auth = FirebaseAuth.getInstance()
    }

    fun forgotPassword(view: View) {
    recoverUserPassword()
    }
    private fun recoverUserPassword(){
        startActivity(Intent(this,RecoverPasswordActivity::class.java))
    }
    fun register (view: View){
        startActivity(Intent(this,RegisterActivity::class.java))
    }
    fun login(view: View){
        loginUser()
    }

    fun loginUser (){
        user = txUser.text.toString()
        val password:String = txtPassword.text.toString()

        if(!TextUtils.isEmpty(user) && (!TextUtils.isEmpty(password))){
            if(password.length < 6){
                Toast.makeText(this,"La contraseña debe tener minimo 6 caracteres", Toast.LENGTH_LONG).show()
            }else{
                progressBar.visibility = View.VISIBLE

                auth.signInWithEmailAndPassword(user,password)
                    .addOnCompleteListener(this){
                            task ->
                        if (task.isSuccessful){
                            Toast.makeText(this,"inicio correcto",Toast.LENGTH_LONG).show()
                            saveData()
                            action()
                        }else{
                            Toast.makeText(this,"Error usuario o clave incorrectos ",Toast.LENGTH_LONG).show()
                            progressBar.visibility = View.INVISIBLE
                        }
                    }

            }
        }else {
            Toast.makeText(this, "Error uno o mas campos vacios", Toast.LENGTH_LONG).show()
        }
    }

    private fun action() {

        startActivity(Intent(this,MainActivity::class.java))


    }
    private fun saveData (){
        val preferencias = getSharedPreferences("datos",Context.MODE_PRIVATE)

        val editor = preferencias.edit()
        editor.putString("user",txtUser.text.toString())
        editor.commit()
        finish()
    }
}
