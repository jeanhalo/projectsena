package co.edu.misena.projectsena

import android.content.Context
import android.content.SharedPreferences

class MyPreferences (context : Context) {

    val USER = "usuarioSharePreference"

    val preference = context.getSharedPreferences(USER,Context.MODE_PRIVATE)

    fun getUsuario (): String? {
        return preference.getString(USER,"")

    }

    fun setUsuario (user : String){
        val editor =preference.edit()
        editor.putString(USER,user)
        editor.apply()
    }

}