package co.edu.misena.projectsena.ui.sorteo

import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Torneo

interface SorteoClickCallback {
    fun onGenerate()
}