package co.edu.misena.projectsena.di

import android.app.Application
import androidx.room.Room
import co.edu.misena.projectsena.api.services.PruebaService
import co.edu.misena.projectsena.db.InitDb
import co.edu.misena.projectsena.db.dao.TorneoDao
import co.edu.misena.projectsena.db.dao.UserDao
import co.edu.misena.projectsena.util.LiveDataCallAdapterFactory
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {
    // Start Services
    @Singleton
    @Provides
    fun provideGithubService(): PruebaService {
        return Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(PruebaService::class.java)
    }
    // End Services

    // Start room and dao
    @Singleton
    @Provides
    fun provideDb(app: Application): InitDb {
        return Room
            .databaseBuilder(app, InitDb::class.java, "projectsena.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: InitDb): UserDao {
        return db.userDao()
    }

    @Singleton
    @Provides
    fun provideTorneoDao(db: InitDb): TorneoDao {
        return db.torneoDao()
    }
    // End room and dao

    // Start FireStore
    @Singleton
    @Provides
    fun provideFirebaseFirestore(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }
    // End FireStore

}
