package co.edu.misena.projectsena.ui.torneo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.databinding.TorneoItemBinding

class TorneoListAdapter(@Nullable clickCallback: TorneoClickCallback) :
    RecyclerView.Adapter<TorneoListAdapter.TorneoViewHolder>() {

    var mTorneoList: List<Torneo?>? = null

    @Nullable
    private var mTorneoClickCallback: TorneoClickCallback? = null

    init {
        mTorneoClickCallback = clickCallback
    }

    fun setTorneoList(torneoList: List<Torneo?>) {
        if (mTorneoList == null) {
            mTorneoList = torneoList
            notifyItemRangeInserted(0, torneoList.size)
        } else {

            val result: DiffUtil.DiffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return mTorneoList!![oldItemPosition]!!.nombreTorneo == torneoList[newItemPosition]?.nombreTorneo
                }

                override fun getOldListSize(): Int {
                    return mTorneoList!!.size
                }

                override fun getNewListSize(): Int {
                    return torneoList.size
                }

                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int
                ): Boolean {
                    val newTorneo: Torneo? = torneoList[newItemPosition]
                    val oldTorneo: Torneo? = mTorneoList!![oldItemPosition]
                    return (newTorneo?.id == oldTorneo?.id && newTorneo?.codigoTorneo == oldTorneo?.codigoTorneo
                            && newTorneo?.nombreTorneo == oldTorneo?.nombreTorneo && newTorneo?.userTorneo == oldTorneo?.userTorneo)
                }

            })
            mTorneoList = torneoList
            result.dispatchUpdatesTo(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TorneoViewHolder {
        val binding: TorneoItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.torneo_item,
            parent,
            false
        )
        binding.callback = mTorneoClickCallback
        return TorneoViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (mTorneoList == null) 0 else mTorneoList!!.size
    }

    override fun onBindViewHolder(holder: TorneoViewHolder, position: Int) {
        holder.binding.torneo = mTorneoList?.get(position)
        holder.binding.executePendingBindings()
    }

    // Init ViewHolder
    class TorneoViewHolder(binding: TorneoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val binding: TorneoItemBinding = binding
    }
}