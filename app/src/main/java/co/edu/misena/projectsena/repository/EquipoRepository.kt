package co.edu.misena.projectsena.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.firebase.CollectionNames.Companion.EQUIPOS
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class EquipoRepository
@Inject constructor(
    firebaseDb: FirebaseFirestore
) {
    companion object {
        const val TAG = "EquipoRepository"
    }

    private var equipoRef: CollectionReference? = null

    init {
        this.equipoRef = firebaseDb.collection(EQUIPOS)
    }

    fun saveData(nombreEquipo: String, torneoId: String): Boolean {
        val equipo = hashMapOf(
            "nombreEquipo" to nombreEquipo,
            "torneoId" to torneoId
        )

        var respuesta = true;

        equipoRef?.add(equipo)
            ?.addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                respuesta = true
            }
            ?.addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
                respuesta = false
            }
        return respuesta
    }


    fun crearData() {

        val equipo1 = hashMapOf(
            "nombreEquipo" to "Equipo1",
            "torneoId" to "TqbgYOZLuGJcZEw3QX3K"
        )

        val equipo2 = hashMapOf(
            "nombreEquipo" to "Equipo2",
            "torneoId" to "rtMZQDwTP2ZFvyC43YK8"
        )

        equipoRef?.add(equipo1)
            ?.addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            ?.addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }

        equipoRef?.add(equipo2)
            ?.addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            ?.addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    /**fun loadEquipos(): MutableLiveData<List<Equipo>> {
        var retornoList: MutableLiveData<List<Equipo>> = MutableLiveData<List<Equipo>>()
        equipoRef?.get()?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val equipoList: MutableList<Equipo> = ArrayList()
                for (document in task.result!!) {
                    val equipo: Equipo = document.toObject(Equipo::class.java)
                    equipo.id = document.id
                    equipoList.add(equipo)
                }
                retornoList.value = equipoList
            } else {
                Timber.d(task.exception);
            }
        }
        return retornoList
    }*/

    /**fun loadEquiposByTorneo(torneoId: String): MutableLiveData<List<Equipo>> {
        var retornoList: MutableLiveData<List<Equipo>> = MutableLiveData<List<Equipo>>()
        equipoRef
            ?.whereEqualTo("torneoId", torneoId)
            ?.get()?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val equipoList: MutableList<Equipo> = ArrayList()
                    for (document in task.result!!) {
                        val equipo: Equipo = document.toObject(Equipo::class.java)
                        equipo.id = document.id
                        equipoList.add(equipo)
                    }
                    retornoList.value = equipoList
                } else {
                    Timber.d(task.exception);
                }
            }
        return retornoList
    }*/

    fun loadEquiposSnapshot(): MutableLiveData<List<Equipo>> {
        var retornoList: MutableLiveData<List<Equipo>> = MutableLiveData<List<Equipo>>()

        equipoRef?.addSnapshotListener { documents, _ ->
            val equipoList: MutableList<Equipo> = ArrayList()
            documents?.forEach { document ->
                val equipo = document.toObject(Equipo::class.java)
                equipo.id = document.id
                equipoList.add(equipo)
            }
            retornoList.value = equipoList.sortedBy { myObj -> myObj.nombreEquipo }
        }

        return retornoList
    }

    fun loadEquiposByTorneoObs(torneoId: String): MutableLiveData<List<Equipo>> {
        var retornoList: MutableLiveData<List<Equipo>> = MutableLiveData<List<Equipo>>()

        equipoRef?.whereEqualTo("torneoId", torneoId)?.addSnapshotListener { documents, _ ->
            val equipoList: MutableList<Equipo> = ArrayList()
            documents?.forEach { document ->
                val equipo = document.toObject(Equipo::class.java)
                equipo.id = document.id
                equipoList.add(equipo)
            }
            retornoList.value = equipoList.sortedBy { myObj -> myObj.nombreEquipo }
        }

        return retornoList
    }

    fun loadEquiposListByTorneo(torneoId: String): MutableLiveData<List<Equipo>> {
        var retornoList: MutableLiveData<List<Equipo>> = MutableLiveData<List<Equipo>>()
        equipoRef
            ?.whereEqualTo("torneoId", torneoId)
            ?.get()?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val equipoList: MutableList<Equipo> = ArrayList()
                    for (document in task.result!!) {
                        val equipo: Equipo = document.toObject(Equipo::class.java)
                        equipo.id = document.id
                        equipoList.add(equipo)
                    }
                    retornoList.value = equipoList
                } else {
                    Timber.d(task.exception);
                }
            }
        return retornoList
    }

    fun loadEquiposList(): MutableLiveData<List<Equipo>> {
        var retornoList: MutableLiveData<List<Equipo>> = MutableLiveData<List<Equipo>>()
        equipoRef
            ?.get()?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val equipoList: MutableList<Equipo> = ArrayList()
                    for (document in task.result!!) {
                        val equipo: Equipo = document.toObject(Equipo::class.java)
                        equipo.id = document.id
                        equipoList.add(equipo)
                    }
                    retornoList.value = equipoList
                } else {
                    Timber.d(task.exception);
                }
            }
        return retornoList
    }

    fun deleteEquipoById(equipo: Equipo) {
        equipoRef?.document(equipo.id)
            ?.delete()
            ?.addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!") }
            ?.addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
    }

}