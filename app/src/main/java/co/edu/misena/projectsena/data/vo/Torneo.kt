package co.edu.misena.projectsena.data.vo

data class Torneo (
    var id: String = "",
    val nombreTorneo: String = "",
    val codigoTorneo: String = "",
    val userTorneo: String = ""
)