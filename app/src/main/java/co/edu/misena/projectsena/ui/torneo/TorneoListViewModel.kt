package co.edu.misena.projectsena.ui.torneo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.db.entity.TorneoEntity
import co.edu.misena.projectsena.repository.TorneoRepository
import javax.inject.Inject

class TorneoListViewModel
@Inject constructor(
    torneoRepository: TorneoRepository
) : ViewModel() {
    private var mtorneoRepository: TorneoRepository? = null

    private val _userId = MutableLiveData<String>()
    val userId: LiveData<String>
        get() = _userId

    private var _torneosObs: LiveData<List<Torneo>>? = null

    init {
        this.mtorneoRepository = torneoRepository
    }


    fun getTorneoObsByUser(): LiveData<List<Torneo>> {
        var liveData = _torneosObs
        if (liveData == null) {
            liveData = mtorneoRepository?.loadTorneosByUser(userId.value.toString())
            _torneosObs = liveData
        }
        return liveData!!
    }

    //val torneos: LiveData<List<Torneo>> = torneoRepository.loadTorneos()

    fun crearData() {
        mtorneoRepository?.crearData()
    }

    fun setUsuarioId(userId: String?) {
        if (_userId.value != userId) {
            _userId.value = userId
        }
    }

    fun deleteTorneoById(torneo: Torneo) {
        mtorneoRepository?.deleteTorneoById(torneo)
    }
}
