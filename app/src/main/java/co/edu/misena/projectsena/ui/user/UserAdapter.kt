package co.edu.misena.projectsena.ui.user

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.databinding.UserItemBinding
import co.edu.misena.projectsena.db.entity.User

class UserAdapter(@Nullable clickCallback: UserClickCallback) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    var mUserList: List<User?>? = null

    @Nullable
    private var mUserClickCallback: UserClickCallback? = null

    init {
        mUserClickCallback = clickCallback
    }

    fun setUserList(userList: List<User?>) {
        if (mUserList == null) {
            mUserList = userList
            notifyItemRangeInserted(0, userList.size)
        } else {

            val result: DiffUtil.DiffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return mUserList!![oldItemPosition]!!.login == userList[newItemPosition]?.login
                }

                override fun getOldListSize(): Int {
                    return mUserList!!.size
                }

                override fun getNewListSize(): Int {
                    return userList.size
                }

                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int
                ): Boolean {
                    val newProduct: User? = userList[newItemPosition]
                    val oldProduct: User? = mUserList!![oldItemPosition]
                    return (newProduct!!.login === oldProduct!!.login && newProduct!!.id == oldProduct!!.id
                            && newProduct.url == oldProduct.url)
                }

            })
            mUserList = userList
            result.dispatchUpdatesTo(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val binding: UserItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.user_item,
            parent,
            false
        )
        binding.callback = mUserClickCallback
        return UserViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (mUserList == null) 0 else mUserList!!.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.binding.user = mUserList?.get(position)
        holder.binding.executePendingBindings()
    }

    // Init ViewHolder
    class UserViewHolder(binding: UserItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val binding: UserItemBinding = binding
    }

}
