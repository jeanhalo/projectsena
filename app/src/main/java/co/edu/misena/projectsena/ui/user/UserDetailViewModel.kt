package co.edu.misena.projectsena.ui.user

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import co.edu.misena.projectsena.data.vo.Resource
import co.edu.misena.projectsena.db.entity.User
import co.edu.misena.projectsena.repository.UserRepository
import co.edu.misena.projectsena.util.AbsentLiveData
import javax.inject.Inject

class UserDetailViewModel
@Inject constructor(
    userRepository: UserRepository
) : ViewModel() {

    private val _login = MutableLiveData<String>()
    val login: LiveData<String>
        get() = _login

    var user: ObservableField<User> = ObservableField()

    private val mUser: LiveData<Resource<User>> = Transformations
        .switchMap(_login) { login ->
            if (login == null) {
                AbsentLiveData.create()
            } else {
                userRepository.loadUser(login)
            }
        }


    fun getObservableUser(): LiveData<Resource<User>>? {
        return this.mUser
    }

    fun setUser(user: User?) {
        this.user.set(user)
    }

    fun setLogin(login: String?) {
        if (_login.value != login) {
            _login.value = login
        }
    }

    fun retry() {
        _login.value?.let {
            _login.value = it
        }
    }


}