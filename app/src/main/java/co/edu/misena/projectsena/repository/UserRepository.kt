package co.edu.misena.projectsena.repository

import androidx.lifecycle.LiveData
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.api.common.ApiResponse
import co.edu.misena.projectsena.api.services.PruebaService
import co.edu.misena.projectsena.data.vo.Resource
import co.edu.misena.projectsena.db.dao.UserDao
import co.edu.misena.projectsena.db.entity.User
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles User objects.
 */
@Singleton
class UserRepository
@Inject constructor(
    private val appExecutors: AppExecutors,
    private val userDao: UserDao,
    private val githubService: PruebaService
) {

    fun loadUser(login: String): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, User>(appExecutors) {
            override fun saveCallResult(item: User) {
                userDao.insert(item)
            }
            override fun shouldFetch(data: User?) = data == null

            override fun loadFromDb() = userDao.findByLogin(login)

            override fun createCall() = githubService.getUser(login)
        }.asLiveData()
    }

    fun loadUsers(): LiveData<Resource<List<User>>> {
        return object : NetworkBoundResource<List<User>, List<User>>(appExecutors) {
            override fun saveCallResult(item: List<User>) {
                userDao.insertAll(item)
            }

            override fun shouldFetch(data: List<User>?) = data?.size == 0

            override fun loadFromDb() = userDao.findAllUsers()

            override fun createCall() = githubService.getAllUser()
        }.asLiveData()
    }
}
