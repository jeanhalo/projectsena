package co.edu.misena.projectsena.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.edu.misena.projectsena.ui.crearequipo.CrearEquipoViewModel
import co.edu.misena.projectsena.ui.creartorneo.CrearTorneoViewModel
import co.edu.misena.projectsena.ui.equipo.EquipoViewModel
import co.edu.misena.projectsena.ui.gestionartorneo.GestionarTorneoViewModel
import co.edu.misena.projectsena.ui.sorteo.SorteoViewModel
import co.edu.misena.projectsena.ui.torneo.TorneoListViewModel
import co.edu.misena.projectsena.ui.user.UserDetailViewModel
import co.edu.misena.projectsena.ui.user.UserViewModel
import co.edu.misena.projectsena.viewmodelfactory.TorneoViewModelFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    abstract fun bindUserViewModel(userViewModel: UserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailViewModel::class)
    abstract fun bindUserDetailViewModel(userDetailViewModel: UserDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TorneoListViewModel::class)
    abstract fun bindTorneoListViewModel(torneoListViewModel: TorneoListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CrearTorneoViewModel::class)
    abstract fun bindCrearTorneoViewModel(crearTorneoViewModel: CrearTorneoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GestionarTorneoViewModel::class)
    abstract fun bindGestionarTorneoViewModel(gestionarTorneoViewModel: GestionarTorneoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EquipoViewModel::class)
    abstract fun bindEquipoViewModel(equipoViewModel: EquipoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CrearEquipoViewModel::class)
    abstract fun bindCrearEquipoViewModel(crearEquipoViewModel: CrearEquipoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SorteoViewModel::class)
    abstract fun bindSorteoViewModel(sorteoViewModel: SorteoViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: TorneoViewModelFactory): ViewModelProvider.Factory
}
