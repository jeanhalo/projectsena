package co.edu.misena.projectsena.ui.equipo

import android.app.Application
import androidx.lifecycle.*
import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.repository.EquipoRepository
import javax.inject.Inject

class EquipoViewModel
@Inject constructor(
    equipoRepository: EquipoRepository,
    application: Application
) : ViewModel() {

    private var mEquipoRepository: EquipoRepository? = null

    private val _torneoId = MutableLiveData<String>()
    val torneoId: LiveData<String>
        get() = _torneoId

    private var _equiposObs: LiveData<List<Equipo>>? = null

    init {
        this.mEquipoRepository = equipoRepository
    }

    fun getEquiposObs(): LiveData<List<Equipo>> {
        var liveData = _equiposObs
        if (liveData == null) {
            liveData = mEquipoRepository?.loadEquiposSnapshot()
            _equiposObs = liveData
        }
        return liveData!!
    }

    fun getEquiposObsForTorneo(): LiveData<List<Equipo>> {
        var liveData = _equiposObs
        if (liveData == null) {
            liveData = mEquipoRepository?.loadEquiposByTorneoObs(torneoId.value.toString())
            _equiposObs = liveData
        }
        return liveData!!
    }

    fun setTorneoId(torneoId: String?) {
        if (_torneoId.value != torneoId) {
            _torneoId.value = torneoId
        }
    }

    fun deleteEquipoById(equipo: Equipo) {
        mEquipoRepository?.deleteEquipoById(equipo)
    }

    /**fun crearData() {
        mEquipoRepository?.crearData()
    }*/

}
