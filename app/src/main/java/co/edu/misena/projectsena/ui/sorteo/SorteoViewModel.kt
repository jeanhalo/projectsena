package co.edu.misena.projectsena.ui.sorteo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Sorteo
import co.edu.misena.projectsena.repository.EquipoRepository
import co.edu.misena.projectsena.repository.SorteoRepository
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class SorteoViewModel
@Inject constructor(
    sorteoRepository: SorteoRepository,
    equipoRepository: EquipoRepository
) : ViewModel() {

    private var mSorteoRepository: SorteoRepository? = null
    private var mEquipoRepository: EquipoRepository? = null

    private val _torneoId = MutableLiveData<String>()
    val torneoId: LiveData<String>
        get() = _torneoId

    private var _sorteosObs: LiveData<List<Sorteo>>? = null

    init {
        this.mEquipoRepository = equipoRepository
        this.mSorteoRepository = sorteoRepository
    }

    fun setTorneoId(torneoId: String?) {
        if (_torneoId.value != torneoId) {
            _torneoId.value = torneoId
        }
    }

    fun getSorteosObsByTorneo(): LiveData<List<Sorteo>> {
        var liveData = _sorteosObs
        if (liveData == null) {
            liveData = mSorteoRepository?.loadSorteoByTorneo(torneoId.value.toString())
            _sorteosObs = liveData
        }
        return liveData!!
    }


    // Funciones para generar enfrentamientos
    private var v1:MutableList<Equipo> = ArrayList()
    private var v2:MutableList<Equipo> = ArrayList()
    var equipos: LiveData<List<Equipo>>? = null
    private var pivote:Equipo? = null

    fun setEquipos(torneoId: String)  {
        equipos = mEquipoRepository?.loadEquiposListByTorneo(torneoId)
    }

    private fun primeraJornada() {
        pivote = equipos?.value?.get(0)

        for (i in 1 until equipos?.value?.size!!) {
            if (i % 2 == 0) {
                v1.add(equipos?.value?.get(i)!!)
            } else {
                v2.add(equipos?.value?.get(i)!!)
            }
        }

        if (equipos?.value!!.size % 2 == 0) {
            for (i in 1 until v2.size) {
                mSorteoRepository?.saveData(v1[i - 1], v2[i], torneoId.value.toString(), 1)
            }
            mSorteoRepository?.saveData(pivote!!, v2[0], torneoId.value.toString(), 1)
        } else {
            for (i in 0 until v2.size) {
                mSorteoRepository?.saveData(v1[i], v2[i], torneoId.value.toString(), 1)
            }
        }
    }

    private  fun rotarEquipos() {
        v2.add(0, v1.first())
        v1.removeAt(0)
        v1.add(v2.last())
        v2.removeAt(v2.size - 1)
    }

    private fun generarJornada(jornada: Int) {
        for (i in 1 until v2.size) {
            mSorteoRepository?.saveData(v1[i - 1], v2[i], torneoId.value.toString(), jornada)
        }

        if (equipos?.value!!.size % 2 == 0) {
            mSorteoRepository?.saveData(pivote!!, v2[0], torneoId.value.toString(), jornada)
        } else {
            mSorteoRepository?.saveData(v1.last(), pivote!!, torneoId.value.toString(), jornada)
        }

    }

    fun generarEnfrentamientos() {
        primeraJornada()
        for (i in 2 until equipos?.value!!.size) {
            rotarEquipos()
            generarJornada(i)
        }
        v1 = ArrayList()
        v2 = ArrayList()
        pivote = null
    }

}
