package co.edu.misena.projectsena.ui.equipo

import android.app.ProgressDialog
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.MainActivity

import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.databinding.EquipoFragmentBinding
import co.edu.misena.projectsena.di.Injectable
import javax.inject.Inject

class EquipoFragment : Fragment(), Injectable {

    companion object {
        const val TAG = "EquipoFragment"
        const val KEY_TORNEO_ON_EQUIPO = "torneo_id"
        fun forTorneoId(torneoId: String): EquipoFragment? {
            val fragment = EquipoFragment()
            val args = Bundle()
            args.putString(KEY_TORNEO_ON_EQUIPO, torneoId)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: EquipoViewModel

    private var mEquipoAdapter: EquipoAdapter? = null

    private var mBinding: EquipoFragmentBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private var progressAlert: ProgressDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.equipo_fragment, container, false)

        mEquipoAdapter = EquipoAdapter(mEquipoClickCallback)
        mBinding?.elEquipoListRv?.adapter = mEquipoAdapter
        mBinding?.callback = mEquipoClickCallback

        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var paramTorneoId = arguments?.getString(KEY_TORNEO_ON_EQUIPO)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(EquipoViewModel::class.java)

        viewModel.setTorneoId(paramTorneoId)
        mBinding?.torneoId = paramTorneoId

        subscribeUi(viewModel)

    }

    private fun subscribeUi(viewModel: EquipoViewModel) {
        mBinding?.isLoading = true
        // Update the list when the data changes
        viewModel.getEquiposObsForTorneo().observe(viewLifecycleOwner, Observer { equipos ->
            if (equipos.isNotEmpty()) {
                mBinding?.isLoading = false
                mEquipoAdapter?.setEquipoList(equipos)
            } else {
                mBinding?.isLoading = false
                mEquipoAdapter?.setEquipoList(equipos)
                mensajeAlert("", "No cuenta con equipos creados.")
            }
            mBinding?.executePendingBindings()
        })

    }

    // Mover entre fragments
    private val mEquipoClickCallback: EquipoClickCallback = object : EquipoClickCallback {
        override fun onClick(equipo: Equipo) {
            println("onClick")
        }

        override fun onNavigate(torneoId: String) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                (activity as MainActivity?)?.showCrearEquipo(torneoId)
            }
        }

        override fun onDeleteTeam(equipo: Equipo) {
            confirmDeleteEquipo(equipo)
        }
    }

    fun confirmDeleteEquipo(equipo: Equipo) {
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        dialogBuilder.setMessage("Esta seguro de desea eliminar el equipo " + equipo.nombreEquipo)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton("Aceptar") { dialog, _ ->
                viewModel.deleteEquipoById(equipo)
            }
            .setNegativeButton("Cancelar") { dialog, _ ->
                dialog.dismiss()
            }
            .setTitle("")

        val alertMensaje = dialogBuilder.create()
        alertMensaje.show()
    }

    fun progressAlert() {
        this.progressAlert = ProgressDialog.show(
            requireActivity(), "",
            "Guardando. Por favor espere...", true
        )
        this.progressAlert?.dismiss()
    }

    fun mensajeAlert(titulo: String, mensaje: String) {
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        dialogBuilder.setMessage(mensaje)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton("Aceptar") { dialog, _ ->
                dialog.dismiss()
            }
            .setTitle(titulo)

        val alertMensaje = dialogBuilder.create()
        alertMensaje.show()
    }

}
