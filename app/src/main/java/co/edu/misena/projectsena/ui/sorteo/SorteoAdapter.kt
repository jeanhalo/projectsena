package co.edu.misena.projectsena.ui.sorteo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Sorteo
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.databinding.EquipoItemBinding
import co.edu.misena.projectsena.databinding.SorteoItemBinding
import co.edu.misena.projectsena.databinding.TorneoItemBinding

class SorteoAdapter(@Nullable clickCallback: SorteoClickCallback) :
    RecyclerView.Adapter<SorteoAdapter.SorteoViewHolder>() {

    var mSorteoList: List<Sorteo?>? = null

    @Nullable
    private var mSorteoClickCallback: SorteoClickCallback? = null

    init {
        mSorteoClickCallback = clickCallback
    }

    fun setSorteoList(sorteoList: List<Sorteo?>) {
        if (mSorteoList == null) {
            mSorteoList = sorteoList
            notifyItemRangeInserted(0, sorteoList.size)
        } else {

            val result: DiffUtil.DiffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return mSorteoList!![oldItemPosition]!!.id == sorteoList[newItemPosition]?.id
                }

                override fun getOldListSize(): Int {
                    return mSorteoList!!.size
                }

                override fun getNewListSize(): Int {
                    return sorteoList.size
                }

                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int
                ): Boolean {
                    val newEquipo: Sorteo? = sorteoList[newItemPosition]
                    val oldEquipo: Sorteo? = mSorteoList!![oldItemPosition]
                    return (newEquipo?.id == oldEquipo?.id && newEquipo?.localId == oldEquipo?.localId
                            && newEquipo?.torneoId == oldEquipo?.torneoId && newEquipo?.visitanteId == oldEquipo?.visitanteId
                            && newEquipo?.jornada == oldEquipo?.jornada)
                }

            })
            mSorteoList = sorteoList
            result.dispatchUpdatesTo(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SorteoViewHolder {
        val binding: SorteoItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.sorteo_item,
            parent,
            false
        )
        binding.callback = mSorteoClickCallback
        return SorteoViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (mSorteoList == null) 0 else mSorteoList!!.size
    }

    override fun onBindViewHolder(holder: SorteoViewHolder, position: Int) {
        holder.binding.sorteo = mSorteoList?.get(position)
        holder.binding.executePendingBindings()
    }

    // Init ViewHolder
    class SorteoViewHolder(binding: SorteoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val binding: SorteoItemBinding = binding
    }
}