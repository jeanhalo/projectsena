package co.edu.misena.projectsena.ui.crearequipo

import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.databinding.CrearEquipoFragmentBinding
import co.edu.misena.projectsena.di.Injectable
import kotlinx.android.synthetic.main.activity_register.view.*
import javax.inject.Inject


class CrearEquipoFragment : Fragment(), Injectable {

    companion object {
        const val TAG = "GestionarTorneoFragment"
        const val KEY_TORNEO_ON_SAVE_TEAM = "torneo_id"
        fun forTorneoId(torneoId: String): CrearEquipoFragment? {
            val fragment = CrearEquipoFragment()
            val args = Bundle()
            args.putString(KEY_TORNEO_ON_SAVE_TEAM, torneoId)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: CrearEquipoViewModel

    private var mBinding: CrearEquipoFragmentBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    // VariableId
    var equipoEditText: EditText? = null
    var progressAlert: ProgressDialog? = null;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.crear_equipo_fragment, container, false)
        mBinding?.callback = mCrearEquipoClickCallback
        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(CrearEquipoViewModel::class.java)

        this.asignarvariables()

    }

    private fun asignarvariables() {
        this.progressAlert()
        equipoEditText = mBinding?.ceEtNombreEquipo
    }

    // Mover entre fragments
    private val mCrearEquipoClickCallback: CrearEquipoClickCallback =
        object : CrearEquipoClickCallback {
            override fun onSaveTeam() {
                println(equipoEditText?.text)
                if (equipoEditText?.text.toString() == "") {
                    mensajeAlert("", "El campo equipo es obligatorio.")
                    return
                }
                var paramTorneoId = arguments?.getString(KEY_TORNEO_ON_SAVE_TEAM)
                progressAlert?.show()
                val respuesta =
                    viewModel.saveData(equipoEditText?.text.toString(), paramTorneoId!!)
                progressAlert?.dismiss()
                if (respuesta!!) {
                    mensajeAlert("", "El equipo se creó exitosamente.")
                    equipoEditText?.setText("")
                } else {
                    mensajeAlert("", "Ocurrió un problema, vuelva a intentarlo.")
                }
            }

            override fun onBack() {
                requireActivity().onBackPressed()
            }

        }


    fun mensajeAlert(titulo: String, mensaje: String) {
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        dialogBuilder.setMessage(mensaje)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton("Aceptar") { dialog, _ ->
                dialog.dismiss()
            }
            .setTitle(titulo)

        val alertMensaje = dialogBuilder.create()
        alertMensaje.show()
    }

    fun progressAlert() {
        this.progressAlert = ProgressDialog.show(
            requireActivity(), "",
            "Guardando. Por favor espere...", true
        )
        this.progressAlert?.dismiss()
    }

}
