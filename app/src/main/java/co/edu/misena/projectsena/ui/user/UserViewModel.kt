package co.edu.misena.projectsena.ui.user

import android.app.Application
import androidx.lifecycle.*
import co.edu.misena.projectsena.data.vo.Resource
import co.edu.misena.projectsena.db.entity.User
import co.edu.misena.projectsena.repository.UserRepository
import co.edu.misena.projectsena.util.AbsentLiveData
import javax.inject.Inject

class UserViewModel
@Inject constructor(
    userRepository: UserRepository
) : ViewModel() {

    val user : LiveData<Resource<User>> = userRepository.loadUser("defunkt")

    val users : LiveData<Resource<List<User>>> = userRepository.loadUsers()

    private val _login = MutableLiveData<String>()
    val login: LiveData<String>
        get() = _login


    val getUsers: LiveData<Resource<List<User>>> = Transformations
        .switchMap(_login) { login ->
            if (login == null) {
                AbsentLiveData.create()
            } else {
                userRepository.loadUsers()
            }
        }

}
