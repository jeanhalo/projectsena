package co.edu.misena.projectsena.data.vo

data class Sorteo (
    var id: String = "",
    val localId: Equipo? = null,
    val visitanteId: Equipo? = null,
    val torneoId: String = "",
    val jornada: Int = 0
)