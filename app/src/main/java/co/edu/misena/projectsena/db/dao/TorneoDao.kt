package co.edu.misena.projectsena.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.edu.misena.projectsena.api.common.ApiResponse
import co.edu.misena.projectsena.db.entity.TorneoEntity

@Dao
interface TorneoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(torneo: TorneoEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(torneos: List<TorneoEntity>)

    @Query("SELECT * FROM torneoEntity WHERE userTorneo = :userTorneo")
    fun findByUser(userTorneo: String): LiveData<List<TorneoEntity>>

    @Query("SELECT * FROM torneoEntity")
    fun findAll(): LiveData<List<TorneoEntity>>

}