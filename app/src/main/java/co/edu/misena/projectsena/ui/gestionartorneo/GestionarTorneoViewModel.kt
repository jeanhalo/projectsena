package co.edu.misena.projectsena.ui.gestionartorneo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.repository.TorneoRepository
import javax.inject.Inject

class GestionarTorneoViewModel
@Inject constructor(
    torneoRepository: TorneoRepository
): ViewModel() {

    var mTorneoRepository: TorneoRepository? = null

    private var _torneoObs: LiveData<Torneo>? = null

    private val _torneoId = MutableLiveData<String>()
    val torneoId: LiveData<String>
        get() = _torneoId

    init {
        this.mTorneoRepository = torneoRepository
    }

    fun getEquiposObs(): LiveData<Torneo> {
        var liveData = _torneoObs
        if (liveData == null) {
            liveData = mTorneoRepository?.loadToneoSnapshot(torneoId.value.toString())
            _torneoObs = liveData
        }
        return liveData!!
    }

    fun setTorneoId(torneoId: String?) {
        if (_torneoId.value != torneoId) {
            _torneoId.value = torneoId
        }
    }

}
