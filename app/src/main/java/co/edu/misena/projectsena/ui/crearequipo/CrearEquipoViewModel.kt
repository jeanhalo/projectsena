package co.edu.misena.projectsena.ui.crearequipo

import androidx.lifecycle.ViewModel
import co.edu.misena.projectsena.repository.EquipoRepository
import javax.inject.Inject

class CrearEquipoViewModel
@Inject constructor(
    equipoRepository: EquipoRepository
): ViewModel() {
    private var mEquipoRepository: EquipoRepository? = null

    init {
        this.mEquipoRepository = equipoRepository
    }

    fun saveData(nombreEquipo: String, torneoId: String): Boolean? {
        return this.mEquipoRepository?.saveData(nombreEquipo, torneoId)
    }
}
