package co.edu.misena.projectsena.ui.torneo

import android.app.ProgressDialog
import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.MainActivity

import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.databinding.TorneoListFragmentBinding
import co.edu.misena.projectsena.db.entity.TorneoEntity
import co.edu.misena.projectsena.di.Injectable
import javax.inject.Inject

class TorneoListFragment : Fragment(), Injectable {

    companion object {
        const val TAG = "TorneoListFragment"
    }

    private lateinit var viewModel: TorneoListViewModel

    private var mTorneoListAdapter: TorneoListAdapter? = null

    private var mBinding: TorneoListFragmentBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private var usuario: String? = null

    private var progressAlert: ProgressDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.torneo_list_fragment, container, false)

        mTorneoListAdapter = TorneoListAdapter(mTorneoClickCallback)
        mBinding?.tlTorneoListRv?.adapter = mTorneoListAdapter
        mBinding?.callback = mTorneoClickCallback

        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(TorneoListViewModel::class.java)

        usuario = cargarPreferencias()!!

        viewModel.setUsuarioId(usuario)

        subscribeUi(viewModel)
    }

    private fun subscribeUi(viewModel: TorneoListViewModel) {
        mBinding?.isLoading = true
        // Update the list when the data changes
        viewModel.getTorneoObsByUser().observe(viewLifecycleOwner, Observer { torneos ->
            if (torneos.isNotEmpty()) {
                mBinding?.isLoading = false
                mTorneoListAdapter?.setTorneoList(torneos)
            } else {
                mBinding?.isLoading = false
                mTorneoListAdapter?.setTorneoList(torneos)
                mensajeAlert("", "No cuenta con torneos creados.")
            }
            mBinding?.executePendingBindings()
        })
    }

    // Mover entre fragments
    private val mTorneoClickCallback: TorneoClickCallback = object : TorneoClickCallback {
        override fun onClick(torneo: Torneo) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                (activity as MainActivity?)?.showGestionarTorneo(torneo!!)
            }
        }

        override fun onNavigate() {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                (activity as MainActivity?)?.showCrearTorneo()
            }
        }

        override fun onDeleteTorneo(torneo: Torneo) {
            confirmDeleteTorneo(torneo)
        }
    }

    private fun cargarPreferencias  (): String? {
        val preferences = this.activity?.getSharedPreferences("datos", Context.MODE_PRIVATE)

        var user : String? = preferences?.getString("user","vacio")
        print (user)
        return user
    }

    fun confirmDeleteTorneo(torneo: Torneo) {
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        dialogBuilder.setMessage("Esta seguro de desea eliminar el torneo " + torneo.nombreTorneo)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton("Aceptar") { dialog, _ ->
                viewModel.deleteTorneoById(torneo)
            }
            .setNegativeButton("Cancelar") { dialog, _ ->
                dialog.dismiss()
            }
            .setTitle("")

        val alertMensaje = dialogBuilder.create()
        alertMensaje.show()
    }

    fun progressAlert() {
        this.progressAlert = ProgressDialog.show(
            requireActivity(), "",
            "Guardando. Por favor espere...", true
        )
        this.progressAlert?.dismiss()
    }

    fun mensajeAlert(titulo: String, mensaje: String) {
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        dialogBuilder.setMessage(mensaje)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton("Aceptar") { dialog, _ ->
                dialog.dismiss()
            }
            .setTitle(titulo)

        val alertMensaje = dialogBuilder.create()
        alertMensaje.show()
    }

}
