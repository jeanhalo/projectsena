package co.edu.misena.projectsena.ui.creartorneo

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.authentication.user
import co.edu.misena.projectsena.databinding.CrearTorneoFragmentBinding
import co.edu.misena.projectsena.di.Injectable
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import javax.inject.Inject
lateinit var nombreTorneo : String
lateinit var clave1 : String
lateinit var clave2 : String
lateinit var usuario: String
class CrearTorneoFragment : Fragment(), Injectable {

    lateinit var txtTournamentName : EditText
    lateinit var txtPassword1 : EditText
    lateinit var txtPassword2 : EditText
    lateinit var mRootReference : DatabaseReference
    lateinit var databaseCloud : FirebaseFirestore

    companion object {
        const val TAG = "CrearTorneoFragment"
    }

    private lateinit var viewModel: CrearTorneoViewModel
    lateinit var  vista : View
    private var mBinding: CrearTorneoFragmentBinding? = null


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vista = inflater.inflate(R.layout.crear_torneo_fragment,container,false)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.crear_torneo_fragment, container, false)
        cargarPreferencias()
        txtTournamentName = vista.findViewById(R.id.etTournamentName)
        txtPassword1 = vista.findViewById(R.id.etPassword)
        txtPassword2 = vista.findViewById(R.id.etPassword2)
        var btnCreate : Button = vista.findViewById(R.id.btnCreateTournament)
        btnCreate.setOnClickListener(){

                cargarData(vista)

        }
        mRootReference = FirebaseDatabase.getInstance().getReference()
        databaseCloud = FirebaseFirestore.getInstance()

        //return mBinding?.root
        return vista

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CrearTorneoViewModel::class.java)
        // TODO: Use the ViewModel
        usuario = cargarPreferencias()!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cargarPreferencias()
        var user : String? = cargarPreferencias()
    }
    private fun validarData (): Boolean{
         nombreTorneo = txtTournamentName.text.toString()
         clave1 = txtPassword1.text.toString()
         clave2 = txtPassword2.text.toString()
        if(!TextUtils.isEmpty(nombreTorneo) && (!TextUtils.isEmpty(clave1)) && (!TextUtils.isEmpty(clave2))){
            Toast.makeText(this.activity ,"validando data", Toast.LENGTH_LONG).show()
            if (clave1 != clave2){
                Toast.makeText(this.activity,"Las contraseñas no coinciden",Toast.LENGTH_LONG).show()
                return false
            }else {
                //Toast.makeText(this.activity,"Las contraseñas  coinciden de forma correcta",Toast.LENGTH_LONG).show()
                return true
            }

        }else {
            Toast.makeText(this.activity,"Error 1 o mas campos vacios",Toast.LENGTH_LONG).show()
            return false
        }

    }
    private fun goToMenu (){
        requireActivity().onBackPressed()
    }

   private fun cargarPreferencias  (): String? {
        val preferences = this.activity?.getSharedPreferences("datos", Context.MODE_PRIVATE)

        var user : String? = preferences?.getString("user","vacio")
        print (user)
       return user
    }

     fun cargarData (view: View){
        if (validarData()==true){
            var stars: MutableMap<String, String> = HashMap()
            stars["userTorneo"] = usuario
            stars["nombreTorneo"] = nombreTorneo
            stars["codigoTorneo"] = clave1


            mRootReference.child("Torneos").push().setValue(stars)
            Toast.makeText(activity,"Datos Guardados satisfactoriamente",Toast.LENGTH_LONG).show()

            databaseCloud.collection("Torneos").document().set(stars)
            goToMenu()



        }else{
            Toast.makeText(activity,"Ocurrio un error al crear el torneo",Toast.LENGTH_LONG).show()

        }
    }

}
