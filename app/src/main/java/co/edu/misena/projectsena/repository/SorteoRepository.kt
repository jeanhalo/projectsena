package co.edu.misena.projectsena.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Sorteo
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.firebase.CollectionNames.Companion.EQUIPOS
import co.edu.misena.projectsena.firebase.CollectionNames.Companion.SORTEOS
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SorteoRepository
@Inject constructor(
    firebaseDb: FirebaseFirestore
) {
    companion object {
        const val TAG = "SorteoRepository"
    }

    private var sorteoRef: CollectionReference? = null

    init {
        this.sorteoRef = firebaseDb.collection(SORTEOS)
    }

    fun saveData(local: Equipo, visitante: Equipo, torneoId: String, jornada: Int) {

        val localMap = hashMapOf(
            "id" to local.id,
            "nombreEquipo" to local.nombreEquipo,
            "torneoId" to local.torneoId
        )

        val visitanteMap = hashMapOf(
            "id" to visitante.id,
            "nombreEquipo" to visitante.nombreEquipo,
            "torneoId" to visitante.torneoId
        )

        val enfrentamiento = hashMapOf(
            "localId" to localMap,
            "visitanteId" to visitanteMap,
            "torneoId" to torneoId,
            "jornada" to jornada
        )

        sorteoRef?.add(enfrentamiento)
            ?.addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            ?.addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    fun loadSorteosSnapshot(): MutableLiveData<List<Sorteo>> {
        var retornoList: MutableLiveData<List<Sorteo>> = MutableLiveData<List<Sorteo>>()

        sorteoRef?.addSnapshotListener { documents, _ ->
            val sorteoList: MutableList<Sorteo> = ArrayList()
            documents?.forEach { document ->
                var sorteo = document.toObject(Sorteo::class.java)
                sorteo.id = document.id
                sorteoList.add(sorteo)
            }
            retornoList.value = sorteoList
        }

        return retornoList
    }

    fun loadSorteoByTorneo(torneoId: String): MutableLiveData<List<Sorteo>> {
        var retornoList: MutableLiveData<List<Sorteo>> = MutableLiveData<List<Sorteo>>()

        sorteoRef?.whereEqualTo("torneoId", torneoId)?.addSnapshotListener { documents, _ ->
            val sorteoList: MutableList<Sorteo> = ArrayList()
            documents?.forEach { document ->
                var sorteo = document.toObject(Sorteo::class.java)
                sorteo.id = document.id
                sorteoList.add(sorteo)
            }
            retornoList.value = sorteoList.sortedBy { myObj -> myObj.jornada }
        }

        return retornoList
    }

}