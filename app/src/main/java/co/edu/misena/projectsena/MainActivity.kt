package co.edu.misena.projectsena

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.db.entity.User
import co.edu.misena.projectsena.ui.crearequipo.CrearEquipoFragment
import co.edu.misena.projectsena.ui.creartorneo.CrearTorneoFragment
import co.edu.misena.projectsena.ui.equipo.EquipoFragment
import co.edu.misena.projectsena.ui.gestionartorneo.GestionarTorneoFragment
import co.edu.misena.projectsena.ui.sorteo.SorteoFragment
import co.edu.misena.projectsena.ui.torneo.TorneoListFragment
import co.edu.misena.projectsena.ui.user.UserDetailFragment
import co.edu.misena.projectsena.ui.user.UserFragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // Add product list fragment if this is first creation
        if (savedInstanceState == null) {
            val fragment = TorneoListFragment()
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, fragment, TorneoListFragment.TAG).commit()
        }
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    /** Shows the example detail fragment  */
    fun showDetailUser(user: User) {
        val userDetailFragment: UserDetailFragment? = UserDetailFragment.forUserLogin(user.login)
        supportFragmentManager
            .beginTransaction()
            .addToBackStack("userDetail")
            .replace(
                R.id.fragment_container,
                userDetailFragment!!,
                null
            ).commit()
    }

    fun showCrearTorneo() {
        val crearTorneoFragment: CrearTorneoFragment? = CrearTorneoFragment()
        supportFragmentManager
            .beginTransaction()
            .addToBackStack("crearTorneo")
            .replace(
                R.id.fragment_container,
                crearTorneoFragment!!,
                null
            ).commit()
    }

    fun showGestionarTorneo(torneo: Torneo) {
        val gestionarTorneoFragment: GestionarTorneoFragment? = GestionarTorneoFragment.forTorneoId(torneo.id)
        supportFragmentManager
            .beginTransaction()
            .addToBackStack("gestionarTorneo")
            .replace(
                R.id.fragment_container,
                gestionarTorneoFragment!!,
                null
            ).commit()
    }

    fun showCrearEquipo(torneoId: String) {
        val crearEquipoFragment: CrearEquipoFragment? = CrearEquipoFragment.forTorneoId(torneoId)
        supportFragmentManager
            .beginTransaction()
            .addToBackStack("crearEquipo")
            .replace(
                R.id.fragment_container,
                crearEquipoFragment!!,
                null
            ).commit()
    }

    fun showEquipos(torneo: Torneo) {
        val equipoFragment: EquipoFragment? = EquipoFragment.forTorneoId(torneo.id)
        supportFragmentManager
            .beginTransaction()
            .addToBackStack("equipos")
            .replace(
                R.id.fragment_container,
                equipoFragment!!,
                null
            ).commit()
    }

    fun showSorteo(torneo: Torneo) {
        val sorteoFragment: SorteoFragment? = SorteoFragment.forTorneoId(torneo.id)
        supportFragmentManager
            .beginTransaction()
            .addToBackStack("sorteo")
            .replace(
                R.id.fragment_container,
                sorteoFragment!!,
                null
            ).commit()
    }

}

