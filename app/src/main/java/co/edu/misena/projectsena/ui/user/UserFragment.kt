package co.edu.misena.projectsena.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.MainActivity
import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Status
import co.edu.misena.projectsena.databinding.ListUserFragmentBinding
import co.edu.misena.projectsena.db.entity.User
import co.edu.misena.projectsena.di.Injectable
import javax.inject.Inject

class UserFragment : Fragment(), Injectable {

    companion object {
        const val TAG = "UserViewModel"
    }

    private var mUserAdapter: UserAdapter? = null

    private var mBinding: ListUserFragmentBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    //var binding by autoCleared<UserItemBinding>()
    //var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    //private var tvLogin: TextView? = null

    private lateinit var viewModel: UserViewModel
    //private var adapter by autoCleared<UserAdapter>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.list_user_fragment, container, false)

        mUserAdapter = UserAdapter(mUserClickCallback)
        mBinding?.luUserListRv?.adapter = mUserAdapter

        return mBinding?.root

        /**val dataBinding = DataBindingUtil.inflate<UserItemBinding>(
            inflater,
            R.layout.user_item,
            container,
            false,
            dataBindingComponent
        )

        binding = dataBinding

        return dataBinding.root*/

        //return inflater.inflate(R.layout.user_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)

        subscribeUi(viewModel)

        /**this.tvLogin = activity?.findViewById(R.id.tvLogin)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        viewModel.user.observe(viewLifecycleOwner, Observer {
        this.tvLogin?.text = it.data?.name
        })*/
    }

    private fun subscribeUi(viewModel: UserViewModel) {
        // Update the list when the data changes
        viewModel.users.observe(viewLifecycleOwner, Observer { usersResource ->
            if (usersResource.status == Status.SUCCESS) {
                mBinding!!.isLoading = false
                mUserAdapter?.setUserList(usersResource.data!!)
            } else {
                mBinding!!.isLoading = true
            }
            mBinding!!.executePendingBindings()
        })
    }

    private val mUserClickCallback: UserClickCallback = object : UserClickCallback {
        override fun onClick(user: User?) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                (activity as MainActivity?)?.showDetailUser(user!!)
            }
        }
    }

}
