package co.edu.misena.projectsena.ui.user

import co.edu.misena.projectsena.db.entity.User

interface UserClickCallback {
    fun onClick(user: User?)
}