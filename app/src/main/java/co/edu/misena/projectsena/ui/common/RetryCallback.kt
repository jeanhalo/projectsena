package co.edu.misena.projectsena.ui.common

/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}
