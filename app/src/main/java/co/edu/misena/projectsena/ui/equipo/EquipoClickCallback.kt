package co.edu.misena.projectsena.ui.equipo

import co.edu.misena.projectsena.data.vo.Equipo
import co.edu.misena.projectsena.data.vo.Torneo

interface EquipoClickCallback {
    fun onClick(equipo: Equipo)
    fun onNavigate(torneoId: String)
    fun onDeleteTeam(equipo: Equipo)
}