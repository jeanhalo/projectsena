package co.edu.misena.projectsena.ui.gestionartorneo

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.MainActivity

import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.databinding.GestionarTorneoFragmentBinding
import co.edu.misena.projectsena.di.Injectable
import co.edu.misena.projectsena.ui.torneo.TorneoClickCallback
import javax.inject.Inject

class GestionarTorneoFragment : Fragment(), Injectable {

    companion object {
        const val TAG = "GestionarTorneoFragment"
        const val KEY_TORNEO = "torneo_id"
        /** Creates gestionar torneo fragment for specific torneo id  */
        fun forTorneoId(torneoId: String): GestionarTorneoFragment? {
            val fragment = GestionarTorneoFragment()
            val args = Bundle()
            args.putString(KEY_TORNEO, torneoId)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: GestionarTorneoViewModel

    private var mBinding: GestionarTorneoFragmentBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.gestionar_torneo_fragment, container, false)

        mBinding?.clickCallback = mGestionarTorneoClickCallback

        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var paramTorneoId = arguments?.getString(KEY_TORNEO)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(GestionarTorneoViewModel::class.java)

        viewModel.setTorneoId(paramTorneoId)

        subscribeUi(viewModel)

    }

    private fun subscribeUi(viewModel: GestionarTorneoViewModel) {
        viewModel.getEquiposObs().observe(viewLifecycleOwner, Observer { torneo ->
            if (torneo == null) {
                Log.d(TAG, "Fallo consultar torneo")
            } else {
                mBinding?.torneo = torneo
            }
        })
    }

    val mGestionarTorneoClickCallback: GestionarTorneosClickCallback = object : GestionarTorneosClickCallback {
        override fun onClick(torneo: Torneo) {
            println("onClick")
        }

        override fun onNavigateEquipos(torneo: Torneo) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                (activity as MainActivity?)?.showEquipos(torneo!!)
            }
        }

        override fun onNavigateSorteo(torneo: Torneo) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                (activity as MainActivity?)?.showSorteo(torneo!!)
            }
        }

        override fun onBack() {
            println("onBack")
        }

    }

}
