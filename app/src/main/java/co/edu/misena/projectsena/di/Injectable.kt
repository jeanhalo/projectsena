package co.edu.misena.projectsena.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
