package co.edu.misena.projectsena.api.services

import androidx.lifecycle.LiveData
import co.edu.misena.projectsena.api.common.ApiResponse
import co.edu.misena.projectsena.db.entity.User
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * REST API access points
 */
interface PruebaService {
    @GET("users/{login}")
    fun getUser(@Path("login") login: String): LiveData<ApiResponse<User>>

    @GET("users")
    fun getAllUser(): LiveData<ApiResponse<List<User>>>
}
