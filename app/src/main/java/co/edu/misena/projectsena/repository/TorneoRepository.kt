package co.edu.misena.projectsena.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.db.dao.TorneoDao
import co.edu.misena.projectsena.db.entity.TorneoEntity
import co.edu.misena.projectsena.firebase.CollectionNames.Companion.TORNEOS
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TorneoRepository
@Inject constructor(
    private val appExecutors: AppExecutors,
    private val torneoDao: TorneoDao,
    firebaseDb: FirebaseFirestore
) {
    companion object {
        const val TAG = "TorneoRepository"
    }

    private var torneoRef: CollectionReference? = null

    /**fun loadTorneosByUser(userId: String): LiveData<List<TorneoEntity>> {
        return torneoDao.findByUser(userId)
    }*/

    init {
        this.torneoRef = firebaseDb.collection(TORNEOS)
    }


    fun crearData() {
        val torneo1 = hashMapOf(
            "nombreTorneo" to "Torneo1",
            "codigoTorneo" to "111",
            "userTorneo" to "user1@correo.com"
        )

        val torneo2 = hashMapOf(
            "nombreTorneo" to "Torneo2",
            "codigoTorneo" to "222",
            "userTorneo" to "user1@correo.com"
        )

        torneoRef?.add(torneo1)
            ?.addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            ?.addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }

        torneoRef?.add(torneo2)
            ?.addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            ?.addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    fun loadTorneos(): MutableLiveData<List<Torneo>> {
        var retornoList: MutableLiveData<List<Torneo>> = MutableLiveData<List<Torneo>>()
        torneoRef?.get()?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val torneoList: MutableList<Torneo> = ArrayList()
                for (document in task.result!!) {
                    val torneo: Torneo = document.toObject(Torneo::class.java)
                    torneo.id = document.id
                    torneoList.add(torneo)
                }
                retornoList.value = torneoList
            } else {
                Timber.d(task.exception);
            }
        }
        return retornoList
    }

    fun loadToneoSnapshot(torneoId: String): MutableLiveData<Torneo> {
        var retorno: MutableLiveData<Torneo> = MutableLiveData<Torneo>()
        torneoRef?.document(torneoId)?.get()?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                var torneoTemp: Torneo
                torneoTemp = task.result?.toObject(Torneo::class.java)!!
                torneoTemp.id = torneoId
                retorno.value = torneoTemp
            } else {
                Timber.d(task.exception);
            }
        }
        return retorno
    }

    fun loadTorneosByUser(userTorneo: String): MutableLiveData<List<Torneo>> {
        var retornoList: MutableLiveData<List<Torneo>> = MutableLiveData<List<Torneo>>()

        torneoRef?.whereEqualTo("userTorneo", userTorneo)?.addSnapshotListener { documents, _ ->
            val torneoList: MutableList<Torneo> = ArrayList()
            documents?.forEach { document ->
                var torneo = document.toObject(Torneo::class.java)
                torneo.id = document.id
                torneoList.add(torneo)
            }
            retornoList.value = torneoList.sortedBy { myObj -> myObj.nombreTorneo }
        }

        return retornoList
    }

    fun deleteTorneoById(torneo: Torneo) {
        torneoRef?.document(torneo.id)
            ?.delete()
            ?.addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!") }
            ?.addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
    }

}