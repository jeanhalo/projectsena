package co.edu.misena.projectsena.ui.sorteo

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.edu.misena.projectsena.AppExecutors
import co.edu.misena.projectsena.MainActivity

import co.edu.misena.projectsena.R
import co.edu.misena.projectsena.databinding.SorteoFragmentBinding
import co.edu.misena.projectsena.di.Injectable
import javax.inject.Inject

class SorteoFragment : Fragment(), Injectable {

    companion object {
        const val TAG = "SorteoFragment"
        const val KEY_TORNEO_ON_SORTEO = "torneo_id"
        fun forTorneoId(torneoId: String): SorteoFragment? {
            val fragment = SorteoFragment()
            val args = Bundle()
            args.putString(KEY_TORNEO_ON_SORTEO, torneoId)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: SorteoViewModel

    private var mSorteoAdapter: SorteoAdapter? = null

    private var mBinding: SorteoFragmentBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.sorteo_fragment, container, false)

        mSorteoAdapter = SorteoAdapter(mSorteoClickCallback)
        mBinding?.sfSorteoListRv?.adapter = mSorteoAdapter
        mBinding?.callback = mSorteoClickCallback

        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var paramTorneoId = arguments?.getString(KEY_TORNEO_ON_SORTEO)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(SorteoViewModel::class.java)

        viewModel.setTorneoId(paramTorneoId)
        mBinding?.torneoId = paramTorneoId

        subscribeUi(viewModel)

    }

    private fun subscribeUi(viewModel: SorteoViewModel) {
        mBinding?.isLoading = true
        // Update the list when the data changes
        viewModel.getSorteosObsByTorneo().observe(viewLifecycleOwner, Observer { sorteos ->
            if (sorteos.isNotEmpty()) {
                mBinding?.isGenerated = false
                mSorteoAdapter?.setSorteoList(sorteos)
            } else {
                mBinding?.isGenerated = true
                mSorteoAdapter?.setSorteoList(sorteos)
            }
            mBinding?.isLoading = false
            mBinding?.executePendingBindings()
        })

        viewModel.setEquipos(arguments?.getString(KEY_TORNEO_ON_SORTEO)!!)
    }

    private val mSorteoClickCallback: SorteoClickCallback = object : SorteoClickCallback {
        override fun onGenerate() {
            println("onGenerate")
            mBinding?.isLoading = true
            if (viewModel.equipos?.value?.size!! >= 3) {
                viewModel.generarEnfrentamientos()
            } else {
                mensajeAlert("", "No puede generar enfrentamientos con menos de 3 equipos.")
                mBinding?.isGenerated = true
                mBinding?.isLoading = false
            }

        }
    }

    fun mensajeAlert(titulo: String, mensaje: String) {
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        dialogBuilder.setMessage(mensaje)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton("Aceptar") { dialog, _ ->
                dialog.dismiss()
            }
            .setTitle(titulo)

        val alertMensaje = dialogBuilder.create()
        alertMensaje.show()
    }

}
