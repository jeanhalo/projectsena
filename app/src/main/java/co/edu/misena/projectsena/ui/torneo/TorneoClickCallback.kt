package co.edu.misena.projectsena.ui.torneo

import co.edu.misena.projectsena.data.vo.Torneo
import co.edu.misena.projectsena.db.entity.TorneoEntity

interface TorneoClickCallback {
    fun onClick(torneo: Torneo)
    fun onNavigate()
    fun onDeleteTorneo(torneo: Torneo)
}