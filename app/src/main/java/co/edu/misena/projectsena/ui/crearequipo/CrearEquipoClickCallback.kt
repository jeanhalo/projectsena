package co.edu.misena.projectsena.ui.crearequipo

import co.edu.misena.projectsena.data.vo.Equipo

interface CrearEquipoClickCallback {
    fun onSaveTeam()
    fun onBack()
}